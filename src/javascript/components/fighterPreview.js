"use strict";
exports.__esModule = true;
exports.createFighterImage = exports.createFighterPreview = void 0;
var domHelper_1 = require("../helpers/domHelper");
function createFighterPreview(fighter, position) {
    var positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    var fighterElement = domHelper_1.createElement({
        tagName: 'div',
        className: "fighter-preview___root " + positionClassName
    });
    var imgElement = createFighterImage(fighter);
    fighterElement.append(imgElement, fighter.name + ' ', fighter.health + ' ', fighter.attack + ' ', fighter.defense + ' ');
    return fighterElement;
}
exports.createFighterPreview = createFighterPreview;
function createFighterImage(fighter) {
    var source = fighter.source, name = fighter.name;
    var attributes = { src: source };
    var imgElement = domHelper_1.createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        title: name,
        alt: name,
        attributes: attributes
    });
    return imgElement;
}
exports.createFighterImage = createFighterImage;
