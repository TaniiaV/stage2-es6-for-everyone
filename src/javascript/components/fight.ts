import { controls } from '../../constants/controls';
import { IFighter } from './interface/interfaces';

let widthIndicator: number = 100;

export async function fight(firstFighter: IFighter, secondFighter: IFighter): Promise<IFighter> {
  let winner1;
  let winner2;
  let winner3;
  let winner4;

  return new Promise((resolve) => {

    document.addEventListener('keydown', function (event) {
      winner1 = runOnKeys(
        event,
        [firstFighter, secondFighter, true],
        controls.PlayerOneAttack
      );

      winner2 = runOnKeys(
        event,
        [secondFighter, firstFighter, false],
        controls.PlayerTwoAttack
      );

      winner3 = runOnKeys(
        event,
        [firstFighter, secondFighter, true],
        controls.PlayerOneCriticalHitCombination
      )

      winner4 = runOnKeys(
        event,
        [secondFighter, firstFighter, false],
        controls.PlayerTWoCriticalHitCombination
      )

      if (winner1) {
        resolve(winner1);
      }

      if (winner2) {
        resolve(winner2);
      }

      if (winner3) {
        resolve(winner3)
      }

      if (winner4) {
        resolve(winner4)
      }

    });

  });
}

function runOnKeys(event: KeyboardEvent, arr: [IFighter, IFighter, boolean], ...codes): IFighter | boolean {
  let pressed: Set<string> = new Set();
  let winner: IFighter | boolean;

  if (event) {
    pressed.add(event.code);

    for (let code of codes) {
      if (!pressed.has(code)) {
        return;
      }
    }

    if (pressed.size === 1 && arr[2]) {
      winner = fighterAttack(arr[0], arr[1], 'right')
    } else if (pressed.size === 1 && !arr[2]) {
      winner = fighterAttack(arr[0], arr[1], 'left')
    } else if (pressed.size === 3 && arr[2]) {
      winner = fighterCriticalAttack(arr[0], arr[1], 'right')
    } else if (pressed.size === 3 && !arr[2]) {
      winner = fighterCriticalAttack(arr[0], arr[1], 'left')
    }

    pressed.clear();
  };

  document.addEventListener('keyup', function (event) {
    pressed.delete(event.code);
  });

  return winner;
}

function fighterAttack(attacker: IFighter, defender: IFighter, value: string): IFighter | boolean {
  const damage = getDamage(attacker, defender);
  const el = document.getElementById(`${value}-fighter-indicator`);

  defender.health -= damage;
  widthIndicator -= damage;
  el.style.width = `${widthIndicator}%`;

  if (defender.health === 0 || defender.health < 0) {
    el.style.width = '0';
    return attacker;
  } else {
    return false;
  }
}

function fighterCriticalAttack(attacker: IFighter, defender: IFighter, value: string): IFighter | boolean {
  const criticalPower = 2 * getHitPower(attacker);
  const el = document.getElementById(`${value}-fighter-indicator`);

  defender.health -= criticalPower;
  widthIndicator -= criticalPower;
  el.style.width = `${widthIndicator}%`;

  if (defender.health === 0 || defender.health < 0) {
    el.style.width = '0';
    return attacker;
  } else {
    return false;
  }
}

function getDamage(attacker: IFighter, defender: IFighter): number {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0, damage);
}

function getPower(min: number, max: number): number {
  return min + Math.random() * (max - min);
}

function getHitPower(fighter: IFighter): number {
  const hitPower = fighter.attack ? fighter.attack * getPower(1, 2) : 0;
  return hitPower;
}

function getBlockPower(fighter: IFighter): number {
  const blockPower = fighter.defense ? fighter.defense * getPower(1, 2) : 0;
  return blockPower;
}
