import { createElement } from '../../helpers/domHelper';

export function showModal(
  { title, bodyElement, onClose = () => { } }: { title: string, bodyElement: string, onClose: () => void }
): void {
  const root = getModalContainer();
  const modal = createModal({ title, bodyElement, onClose });

  if (root) {
    root.append(modal);
  }
}

function getModalContainer(): HTMLElement | null {
  return document.getElementById('root');
}

function createModal(
  { title, bodyElement, onClose }: { title: string, bodyElement: string, onClose: () => void }
): HTMLElement {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose: () => void): HTMLElement {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });

  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close = (): void => {
    hideModal();
    onClose();
  }
  closeButton.addEventListener('click', close);
  headerElement.append(title, closeButton);

  return headerElement;
}

function hideModal(): void {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
