"use strict";
exports.__esModule = true;
exports.showWinnerModal = void 0;
var modal_1 = require("./modal");
function showWinnerModal(fighter) {
    var close = function () {
        setTimeout(function () { }, 10000);
    };
    modal_1.showModal({ title: fighter.name, bodyElement: 'Congratulations! You are a winner <3', onClose: close });
}
exports.showWinnerModal = showWinnerModal;
