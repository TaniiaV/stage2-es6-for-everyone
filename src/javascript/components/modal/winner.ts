import { showModal } from './modal';
import { IFighter } from '../interface/interfaces';

export function showWinnerModal(fighter: IFighter): void {
  let close = (): void => {
    setTimeout(() => { }, 10000);
  }
  showModal({ title: fighter.name, bodyElement: 'Congratulations! You are a winner <3', onClose: close });
}
