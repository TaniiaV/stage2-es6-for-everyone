"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.fight = void 0;
var controls_1 = require("../../constants/controls");
var widthIndicator = 100;
function fight(firstFighter, secondFighter) {
    return __awaiter(this, void 0, void 0, function () {
        var winner1, winner2, winner3, winner4;
        return __generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (resolve) {
                    document.addEventListener('keydown', function (event) {
                        winner1 = runOnKeys(event, [firstFighter, secondFighter, true], controls_1.controls.PlayerOneAttack);
                        winner2 = runOnKeys(event, [secondFighter, firstFighter, false], controls_1.controls.PlayerTwoAttack);
                        winner3 = runOnKeys(event, [firstFighter, secondFighter, true], controls_1.controls.PlayerOneCriticalHitCombination);
                        winner4 = runOnKeys(event, [secondFighter, firstFighter, false], controls_1.controls.PlayerTWoCriticalHitCombination);
                        if (winner1) {
                            resolve(winner1);
                        }
                        if (winner2) {
                            resolve(winner2);
                        }
                        if (winner3) {
                            resolve(winner3);
                        }
                        if (winner4) {
                            resolve(winner4);
                        }
                    });
                })];
        });
    });
}
exports.fight = fight;
function runOnKeys(event, arr) {
    var codes = [];
    for (var _i = 2; _i < arguments.length; _i++) {
        codes[_i - 2] = arguments[_i];
    }
    var pressed = new Set();
    var winner;
    if (event) {
        pressed.add(event.code);
        for (var _a = 0, codes_1 = codes; _a < codes_1.length; _a++) {
            var code = codes_1[_a];
            if (!pressed.has(code)) {
                return;
            }
        }
        if (pressed.size === 1 && arr[2]) {
            winner = fighterAttack(arr[0], arr[1], 'right');
        }
        else if (pressed.size === 1 && !arr[2]) {
            winner = fighterAttack(arr[0], arr[1], 'left');
        }
        else if (pressed.size === 3 && arr[2]) {
            winner = fighterCriticalAttack(arr[0], arr[1], 'right');
        }
        else if (pressed.size === 3 && !arr[2]) {
            winner = fighterCriticalAttack(arr[0], arr[1], 'left');
        }
        pressed.clear();
    }
    ;
    document.addEventListener('keyup', function (event) {
        pressed["delete"](event.code);
    });
    return winner;
}
function fighterAttack(attacker, defender, value) {
    var damage = getDamage(attacker, defender);
    var el = document.getElementById(value + "-fighter-indicator");
    defender.health -= damage;
    widthIndicator -= damage;
    el.style.width = widthIndicator + "%";
    if (defender.health === 0 || defender.health < 0) {
        el.style.width = '0';
        return attacker;
    }
    else {
        return false;
    }
}
function fighterCriticalAttack(attacker, defender, value) {
    var criticalPower = 2 * getHitPower(attacker);
    var el = document.getElementById(value + "-fighter-indicator");
    defender.health -= criticalPower;
    widthIndicator -= criticalPower;
    el.style.width = widthIndicator + "%";
    if (defender.health === 0 || defender.health < 0) {
        el.style.width = '0';
        return attacker;
    }
    else {
        return false;
    }
}
function getDamage(attacker, defender) {
    var damage = getHitPower(attacker) - getBlockPower(defender);
    return Math.max(0, damage);
}
function getPower(min, max) {
    return min + Math.random() * (max - min);
}
function getHitPower(fighter) {
    var hitPower = fighter.attack ? fighter.attack * getPower(1, 2) : 0;
    return hitPower;
}
function getBlockPower(fighter) {
    var blockPower = fighter.defense ? fighter.defense * getPower(1, 2) : 0;
    return blockPower;
}
