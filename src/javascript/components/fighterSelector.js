"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.getFighterInfo = exports.createFightersSelector = void 0;
var domHelper_1 = require("../helpers/domHelper");
var arena_1 = require("./arena");
var fighterPreview_1 = require("./fighterPreview");
var fightersService_1 = require("../services/fightersService");
var versusImg = require('../../../resources/versus.png');
function createFightersSelector() {
    var _this = this;
    var selectedFighters = [];
    return function (event, fighterId) { return __awaiter(_this, void 0, void 0, function () {
        var fighter, playerOne, playerTwo, firstFighter, secondFighter;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, getFighterInfo(fighterId)];
                case 1:
                    fighter = _a.sent();
                    playerOne = selectedFighters[0], playerTwo = selectedFighters[1];
                    firstFighter = playerOne !== null && playerOne !== void 0 ? playerOne : fighter;
                    secondFighter = Boolean(playerOne) ? playerTwo !== null && playerTwo !== void 0 ? playerTwo : fighter : playerTwo;
                    selectedFighters = [firstFighter, secondFighter];
                    renderSelectedFighters(selectedFighters);
                    return [2 /*return*/];
            }
        });
    }); };
}
exports.createFightersSelector = createFightersSelector;
var fighterDetailsMap = new Map();
function getFighterInfo(fighterId) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, fightersService_1.fighterService.getFighterDetails(fighterId).then(function (response) {
                        fighterDetailsMap.set(fighterId, response);
                    })];
                case 1:
                    _a.sent();
                    return [2 /*return*/, fighterDetailsMap.get(fighterId)];
            }
        });
    });
}
exports.getFighterInfo = getFighterInfo;
function renderSelectedFighters(selectedFighters) {
    var fightersPreview = document.querySelector('.preview-container___root');
    var playerOne = selectedFighters[0], playerTwo = selectedFighters[1];
    var firstPreview = fighterPreview_1.createFighterPreview(playerOne, 'left');
    var secondPreview = fighterPreview_1.createFighterPreview(playerTwo, 'right');
    var versusBlock = createVersusBlock(selectedFighters);
    if (fightersPreview) {
        fightersPreview.innerHTML = '';
        fightersPreview.append(firstPreview, versusBlock, secondPreview);
    }
}
function createVersusBlock(selectedFighters) {
    var canStartFight = selectedFighters.filter(Boolean).length === 2;
    var onClick = function () { return startFight(selectedFighters); };
    var container = domHelper_1.createElement({ tagName: 'div', className: 'preview-container___versus-block' });
    var image = domHelper_1.createElement({
        tagName: 'img',
        className: 'preview-container___versus-img',
        attributes: { src: versusImg }
    });
    var disabledBtn = canStartFight ? '' : 'disabled';
    var fightBtn = domHelper_1.createElement({
        tagName: 'button',
        className: "preview-container___fight-btn " + disabledBtn
    });
    fightBtn.addEventListener('click', onClick, false);
    fightBtn.innerText = 'Fight';
    container.append(image, fightBtn);
    return container;
}
function startFight(selectedFighters) {
    arena_1.renderArena(selectedFighters);
}
