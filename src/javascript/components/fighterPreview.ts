import { createElement } from '../helpers/domHelper';
import { IFighter } from './interface/interfaces';

export function createFighterPreview(fighter: IFighter, position: string): HTMLElement {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const imgElement = createFighterImage(fighter);
  fighterElement.append(imgElement, fighter.name + ' ', fighter.health + ' ', fighter.attack + ' ', fighter.defense + ' ');
  return fighterElement;
}

export function createFighterImage(fighter: IFighter): HTMLElement {
  const { source, name } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    title: name,
    alt: name,
    attributes,
  });

  return imgElement;
}
