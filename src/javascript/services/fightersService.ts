import { IFighter } from './../components/interface/interfaces';
import { callApiAll, callApiOne } from '../helpers/apiHelper';

class FighterService {
  async getFighters(): Promise<IFighter[]> {
    try {
      const endpoint = 'fighters.json';
      const apiResult: IFighter[] = await callApiAll(endpoint, 'GET');

      if (apiResult) {
        return apiResult;
      } else {
        return Promise.reject(Error('Failed to load'));
      }
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<IFighter> {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult: IFighter = await callApiOne(endpoint, 'GET');

      if (apiResult) {
        return apiResult;
      } else {
        return Promise.reject(Error('Failed to load'));
      }
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService: FighterService = new FighterService();
