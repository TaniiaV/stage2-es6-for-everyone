import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
import { IFighter } from './components/interface/interfaces';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement: HTMLElement = document.getElementById('root');
  static loadingElement: HTMLElement = document.getElementById('loading-overlay');

  async startApp(): Promise<void> {
    try {
      if (App.loadingElement) {
        App.loadingElement.style.visibility = 'visible';
      }

      const fighters: IFighter[] = await fighterService.getFighters();
      
      if (App.rootElement && fighters) {
        const fightersElement = createFighters(fighters);
        App.rootElement.appendChild(fightersElement);
      }
    } catch (error) {
      console.warn(error);
      if (App.rootElement) {
        App.rootElement.innerText = 'Failed to load data';
      }
    } finally {
      if (App.loadingElement) {
        App.loadingElement.style.visibility = 'hidden';
      }
    }
  }
}

export default App;
