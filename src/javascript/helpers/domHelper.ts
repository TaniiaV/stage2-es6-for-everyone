export function createElement(
  { tagName, className, title, alt, attributes = {} }: { tagName: string, className?: string, title?: string, alt?: string, attributes?: { [key: string]: string } }
): HTMLElement {
  const element: HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
